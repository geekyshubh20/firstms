package com.adv.firstms.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adv.firstms.dto.EmployeeDto;
import com.adv.firstms.service.EmployeeService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
//@Controller
@RequestMapping(value = "/employees")
public class EmployeeController {

	private static final Logger LOGGER = LogManager.getLogger(EmployeeController.class);
	
	@Autowired
	@Qualifier(value = "empSrvImpl")
	private EmployeeService employeeService;

	// sorting
	@Operation(summary = "Get all the employee of the organization")
	@GetMapping
	//@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public ResponseEntity<List<EmployeeDto>> getAllEmployees(@RequestParam(name = "sortby") String sortBy) {
		List<EmployeeDto> employees;
		employees = employeeService.getAllEmployees(sortBy);
		LOGGER.info("employees------->"+employees);
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}

	@GetMapping(value = "/{empId}")
	public ResponseEntity<EmployeeDto> getParticularEmployee(@PathVariable int empId) {
		EmployeeDto employee = employeeService.getParticularEmployee(empId);
		LOGGER.info("employee in getParticularEmployee ------->",employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Void> createEmployee(@RequestBody EmployeeDto employeeDto) {
		employeeService.createEmployee(employeeDto);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping(value = "/{empId}")
	public ResponseEntity<Void> updateEmployee(@PathVariable int empId, @RequestBody EmployeeDto employeeDto) {
		employeeService.updateEmployee(empId, employeeDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

//	@PatchMapping(value = "/employees/{empId}")
//	public ResponseEntity<Void> selUpdEmployee(@PathVariable int empId, @RequestBody EmployeeDto employeeDto) {
//		employeeService.updateEmployee(empId, employeeDto);
//		return new ResponseEntity<>(HttpStatus.OK);
//	}

	@DeleteMapping(value = "/{empId}")
	public ResponseEntity<Void> deleteEmployee(@PathVariable int empId) {
		employeeService.deleteEmployee(empId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
