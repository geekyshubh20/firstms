package com.adv.firstms.sort;

import java.util.Comparator;

import com.adv.firstms.dto.EmployeeDto;

public class NameSort implements Comparator<EmployeeDto> {

	@Override
	public int compare(EmployeeDto o1, EmployeeDto o2) {
		return o1.getName().compareTo(o2.getName());
	}
}
