package com.adv.firstms.service;

import java.util.List;

import com.adv.firstms.dto.EmployeeDto;
import com.adv.firstms.exception.InvalidInputException;

public interface EmployeeService {

	public List<EmployeeDto> getAllEmployees(String sortBy);
	
	public EmployeeDto getParticularEmployee(int empId);
	
	public void createEmployee(EmployeeDto employeeDto);
	
	public void updateEmployee(int empId, EmployeeDto employeeDto);
	
	//public EmployeeDto selUpdEmployee(int empId, EmployeeDto employeeDto);
	
	public void deleteEmployee(int empId);
}
