package com.adv.firstms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adv.firstms.dto.StaffDto;
import com.adv.firstms.entity.StaffEntity;
import com.adv.firstms.repository.StaffRepository;
import com.adv.firstms.service.StaffService;

@Service(value = "staffServiceImpl")
public class StaffServiceImpl implements StaffService {

	@Autowired
	private StaffRepository staffRepository;

	@Override
	public List<StaffDto> getAllStaff() {
		List<StaffEntity> staffEntities = staffRepository.findAll();
		List<StaffDto> staffDtos = new ArrayList<>();
		for (StaffEntity staffEntity : staffEntities) {
			long id = staffEntity.getId();
			String name = staffEntity.getName();
			String email = staffEntity.getEmail();
			StaffDto staffDto = new StaffDto(id, name, email);
			staffDtos.add(staffDto);
		}
		return staffDtos;
	}

	@Override
	public void deleteStaff(long empId) {
		staffRepository.deleteById(empId);
	}
}
