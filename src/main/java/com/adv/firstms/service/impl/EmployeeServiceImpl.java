package com.adv.firstms.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.adv.firstms.dto.EmployeeDto;
import com.adv.firstms.exception.ResourceNotFoundException;
import com.adv.firstms.exception.SortNotSupportedException;
import com.adv.firstms.exception.UnExpectedCustomException;
import com.adv.firstms.service.EmployeeService;
import com.adv.firstms.sort.AgeSort;
import com.adv.firstms.sort.EmailSort;
import com.adv.firstms.sort.NameSort;

@Service(value = "empSrvImpl")
public class EmployeeServiceImpl implements EmployeeService {

	private static final Logger LOGGER = LogManager.getLogger(EmployeeServiceImpl.class);
	
	@Override
	public List<EmployeeDto> getAllEmployees(String sortBy) {
		List<EmployeeDto> employees = new ArrayList<>();
		Connection conn = null;
		try {
			conn = getConnection();
			// 3. Create Statement
			Statement stmt = conn.createStatement();
			// 4. Execute Query
			ResultSet rs = stmt.executeQuery("SELECT id, name, age, email, std, section FROM training08.employee");
			// 5. iterate the resultset
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String age = rs.getString("age");
				String email = rs.getString("email");
				String standard = rs.getString("std");
				String section = rs.getString("section");
				EmployeeDto employee = new EmployeeDto(id, name, id, email, standard, section);
				employees.add(employee);
				LOGGER.debug("Employee value in each row ::"+employee);
			}
		} catch (ClassNotFoundException e) {
			//e.printStackTrace();
			LOGGER.error("Eexception in getAllEmployees::"+e.getMessage());
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpeced Error");
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpeced Error");
				}
			}
		}
		sortByType(employees, sortBy);
		return employees;
	}
	
	public void sortByType(List<EmployeeDto> employeeDtos, String sortBy) {
		switch (sortBy) {
		case "name": 
			Collections.sort(employeeDtos, new NameSort());
			break;
		case "age": 
			Collections.sort(employeeDtos, new AgeSort());
			break;
		case "email": 
			Collections.sort(employeeDtos, new EmailSort());
			break;
		default:
			throw new SortNotSupportedException("FMS001", "Given sort is not supported");
		}
	}
	
	@Override
	public EmployeeDto getParticularEmployee(int empId) {
		EmployeeDto employee = null;
		Connection conn = null;
		try {
			conn = getConnection();
			// 3. Create Statement
			Statement stmt = conn.createStatement();
			// 4. Execute Query
			ResultSet rs = stmt.executeQuery("SELECT id, name, age, email, std, section FROM training08.employee  where id="+empId);
			// 5. iterate the resultset
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String age = rs.getString("age");
				String email = rs.getString("email");
				String standard = rs.getString("std");
				String section = rs.getString("section");
				employee = new EmployeeDto(id, name, id, email, standard, section);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpeced Error");
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpeced Error");
				}
			}
		}
		if (employee == null) {
			throw new ResourceNotFoundException("FMS002", "The resource is not found");
		}
		return employee;
	}

	@Override
	public void createEmployee(EmployeeDto employeeDto) {
		Connection conn = null;
		try {
			conn = getConnection();
			// 3. Create Statement
			PreparedStatement psStmt = conn.prepareStatement("insert into training08.employee (name, age, email, std, section) values(?, ?, ?, ?, ?)");
			psStmt.setString(1, employeeDto.getName());
			psStmt.setInt(2, employeeDto.getAge());
			psStmt.setString(3, employeeDto.getEmail());
			psStmt.setString(4, employeeDto.getStandard());
			psStmt.setString(5, employeeDto.getSection());
			// 4. Execute Query
			boolean res = psStmt.execute();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpeced Error");
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpeced Error");
				}
			}
		}
	}

	@Override
	public void updateEmployee(int empId, EmployeeDto employeeDto) {
		Connection conn = null;
		int newEmpId = 0;
		try {
			conn = getConnection();
			// 3. Create Statement
			PreparedStatement psStmt = conn.prepareStatement("update training08.employee set name=?, age=?, email=?, std=?, section=?) where id=?");
			psStmt.setString(1, employeeDto.getName());
			psStmt.setInt(2, employeeDto.getAge());
			psStmt.setString(3, employeeDto.getEmail());
			psStmt.setString(4, employeeDto.getStandard());
			psStmt.setString(5, employeeDto.getSection());
			psStmt.setInt(6, empId);
			// 4. Execute Query
			newEmpId = psStmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpeced Error");
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpeced Error");
				}
			}
		}
		//return getParticularEmployee(newEmpId);
	}

//	@Override
//	public EmployeeDto selUpdEmployee(int empId, EmployeeDto employeeDto) {
//		Connection conn = null;
//		try {
//			conn = getConnection();
//			// 3. Create Statement
//			PreparedStatement psStmt = conn.prepareStatement("update training08.employee set name=?, age=?, email=?, std=?, section=?) where id=?");
//			psStmt.setString(1, employeeDto.getName());
//			psStmt.setInt(2, employeeDto.getAge());
//			psStmt.setString(3, employeeDto.getEmail());
//			psStmt.setString(4, employeeDto.getStandard());
//			psStmt.setString(5, employeeDto.getSection());
//			psStmt.setInt(6, empId);
//			// 4. Execute Query
//			boolean res = psStmt.execute();
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} finally {
//			// 6. Closing the connection
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//	}

	@Override
	public void deleteEmployee(int empId) {
		Connection conn = null;
		try {
			conn = getConnection();
			// 3. Create Statement
			PreparedStatement psStmt = conn.prepareStatement("delete FROM training08.employee where id=?");
			psStmt.setInt(1, empId);
			// 4. Execute Query
			boolean res = psStmt.execute();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			throw new UnExpectedCustomException("Unexpeced Error");
		} finally {
			// 6. Closing the connection
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					throw new UnExpectedCustomException("Unexpeced Error");
				}
			}
		}
	}

	private Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection conn;
		// 1. Load Driver
		Class.forName("com.mysql.cj.jdbc.Driver");
		// 2. Establish Connection
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/training08", "root", "root");
		return conn;
	}
}
