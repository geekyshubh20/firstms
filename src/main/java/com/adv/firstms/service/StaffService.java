package com.adv.firstms.service;

import java.util.List;

import com.adv.firstms.dto.StaffDto;

public interface StaffService {

	public List<StaffDto> getAllStaff();
	
	public void deleteStaff(long staffId);
}
